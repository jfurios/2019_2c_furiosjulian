/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../4_guia1_INTEG_C/inc/main.h"

/*==================[macros and definitions]=================================*/
void decimal_bcd(uint8_t dat){
		int i=0;
		uint8_t aux[4];
	for(i=3;i>=0;i--){
		aux[i]=dat%2;
		dat/=2;
	}
	for(i=0;i<4;i++)
		printf("%d",aux[i]);
	printf(" ");
	return;
}
void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t *bcd_number )
{

	int i=0;
	for(i=0;i<digits;i++){
		bcd_number[i]=data%10;
		//printf("%d",bcd_number[i]);

		data/=10;
	}
	for(i=(digits)-1;i>=0;i--){
			decimal_bcd(bcd_number[i]);
	}
	return;
}


/*==================[internal functions declaration]=========================*/

int main(void)
{
 uint32_t dato;
 uint8_t dig, bcd[32];

dato=1234567890;
dig=10;

BinaryToBcd(dato,dig,bcd);
	return 0;
}

/*==================[end of file]============================================*/

