
#ifndef _MAIN_H
#define _MAIN_H
/*==================[inclusions]=============================================*/

#include <stdio.h>
#include <stdint.h>


/*==================[macros and definitions]=================================*/
int main(void);

typedef struct
{
    char * txt ;      	/* Etiqueta de la opción */
    void (*doit)() ;             /* Función a ejecutar en caso de seleccionar esa opción */
} menuEntry;

void entrar(int a);
void salir();
void guardar();
void ejecutaMenu(menuEntry *menu, int op);

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /*  */

