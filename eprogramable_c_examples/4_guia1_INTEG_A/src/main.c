/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../4_guia1_INTEG_A/inc/main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/

const uint32_t ct=23;

void Control_LEDS(leds aux){
	if (aux.MODE == 1) {
		if (aux.n_led==1)
			printf("Enciende LED 1\n");
		if (aux.n_led==2)
			printf("Enciende LED 2 \n");
		if (aux.n_led==3)
			printf("Enciende LED 3 \n");
		if (aux.n_led!=1 && aux.n_led!=2 && aux.n_led!=3)
			printf("El led elegido no existe jejejejeje\n");

		return;
		} // Termina modo ON
	if (aux.MODE == 2) {
			if (aux.n_led==1)
				printf("APAGA LED 1\n");
			if (aux.n_led==2)
				printf("APAGA LED 2 \n");
			if (aux.n_led==3)
				printf("APAGA LED 3 \n");
			if (aux.n_led!=1 && aux.n_led!=2 && aux.n_led!=3)
				printf("El led elegido no existe jejejejeje\n");

			return;
	}

	uint8_t j=0;

	if(aux.MODE == 3 )
		for(j=0;j<aux.n_ciclos;j++){
			if (aux.n_led==1)
							printf("toggle LED 1\n");
						if (aux.n_led==2)
							printf("toggle LED 2 \n");
						if (aux.n_led==3)
							printf("toggle LED 3 \n");
						if (aux.n_led!=1 && aux.n_led!=2 && aux.n_led!=3)
						printf("El led elegido no existe jejejejeje toggle :(\n");
			printf("delay de %d segundos\n", aux.periodo);
		}
		 return;
};
int main(void)
{
	leds myleds;

	myleds.MODE=1;
	myleds.n_led=2;
	myleds.n_ciclos=5;
	myleds.periodo=1;

	Control_LEDS(myleds);

   // while(1)
    //{
    //}
	return 0;
}

/*==================[end of file]============================================*/

