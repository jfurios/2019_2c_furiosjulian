/*
 * DisplayITS_E0803.c
 *
 *  Created on: 6 sep. 2019
 *      Author: Alumno
 */

/*==================[inclusions]=============================================*/
#include "gpio.h"
#include "DisplayITS_E0803.h"

/*==================[internal data definition]===============================*/
#define DIGITOS 3
#define BitBcd 4
#define N_pines 7
/*==================[internal data definition]===============================*/
gpio_t Mpines[N_pines];
uint16_t VALOR_AUX;
/*==================[internal functions definition]==========================*/

void DigitoABcd (uint16_t dato, uint8_t bcd_num[3][4] )
{
	int i=0,j=0;
	uint16_t aux;

	for(i=0;i<DIGITOS;i++){
		aux=dato%10;
		dato/=10;
	for(j=0;j<BitBcd;j++){
		bcd_num[i][j]=aux%2;
		aux/=2;
	}
	}
	return;
}

/*==================[external functions definition]==========================*/
/*
 * pins tiene que se un vetor con 7 elementos donde estan ordenados primero los pines de seleccion de LSB a MSB
 * y luegos los 4 pines de LCD de MSB a LSB
 */
bool ITSE0803Init(gpio_t * pins){

	int i=0;

	for(i=0;i<N_pines;i++){
		GPIOInit(pins[i], GPIO_OUTPUT);
		GPIOOff(pins[i]);
		Mpines[i]=pins[i];
	}
/*	uint8_t result=false;
	switch (pins)
		{
		case LCD1:
			GPIOInit(pins, GPIO_OUTPUT);
			GPIOOff(pins);
			result = true;
		break;
		case LCD2:
			GPIOInit(pins, GPIO_OUTPUT);
			GPIOOff(pins);
			result = true;
		break;
		case LCD3:
			GPIOInit(pins, GPIO_OUTPUT);
			GPIOOff(pins);
			result = true;
		break;
		case LCD4:
			GPIOInit(pins, GPIO_OUTPUT);
			GPIOOff(pins);
			result = true;
		break;
		case GPIO1:
			GPIOInit(pins, GPIO_OUTPUT);
			GPIOOff(pins);
			result = true;
		break;
		case GPIO3:
			GPIOInit(pins, GPIO_OUTPUT);
			GPIOOff(pins);
			result = true;
		break;
		case GPIO5:
			GPIOInit(pins, GPIO_OUTPUT);
			GPIOOff(pins);
			result = true;
		break;
		}

	return result;*/
}


bool ITSE0803DisplayValue(uint16_t valor){

	VALOR_AUX=valor;
	uint8_t Digitos_BCD[3][4];
	DigitoABcd(valor, Digitos_BCD);
	uint8_t i=0,j=0;

	for(i=0;i<DIGITOS;i++){
		for(j=0;j<BitBcd;j++){
			if(Digitos_BCD[i][j]==1)
				GPIOOn(Mpines[j+DIGITOS]);
			else
				GPIOOff(Mpines[j+DIGITOS]);
		}
		GPIOOn(Mpines[i]);
		asm  ("nop");
		GPIOOff(Mpines[i]);
	}



}


uint16_t ITSE0803ReadValue(void){
	return VALOR_AUX;
}


bool ITSE0803Deinit(gpio_t * pins){

}
void ITSE0803White(void){
	int i,j;
	for(i=0;i<DIGITOS;i++){
			for(j=0;j<BitBcd;j++){
				GPIOOn(Mpines[j+DIGITOS]);
			}

	GPIOOn(Mpines[i]);
	asm  ("nop");
	GPIOOff(Mpines[i]);
	}
}


/*==================[end of file]============================================*/
