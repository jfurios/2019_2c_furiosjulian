/*
 * DisplayITS_E0803.h
 *
 *  Created on: 6 sep. 2019
 *      Author: Alumno
 */

#ifndef MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_DISPLAYITS_E0803_H_
#define MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_DISPLAYITS_E0803_H_

/*==================[inclusions]=============================================*/
#include "bool.h"
#include <stdint.h>


/** @brief La funciona inicializa un puerto de salida
 *
 * @param[in] pins
 *
 * @return FALSE si se produjo un error, en otro caso retorna TRUE
 */
bool ITSE0803Init(gpio_t * pins);
/** @brief La funciona coloca en el display un valor de hasta 3 digitos decimales
 *
 * @param[in] valor
 *
 * @return FALSE si se produjo un error, en otro caso retorna TRUE
 */
bool ITSE0803DisplayValue(uint16_t valor);
/** @brief La funciona retorna el valor en el que se encuentra el display
 *
 * @param[in] Sin parametros
 *
 * @return variable entera de 16bit (uint16_t)
 */
uint16_t ITSE0803ReadValue(void);
/** @brief La funciona desinicializa un puerto de salida
 *
 * @param[in] pins
 *
 * @return FALSE si se produjo un error, en otro caso retorna TRUE
 */
bool ITSE0803Deinit(gpio_t * pins);
/** @brief La funciona pone en balnco el display
 *
 * @param[in] vacio
 *
 * @return No retorna valor
 */
void ITSE0803White(void);




#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_DISPLAYITS_E0803_H_ */
