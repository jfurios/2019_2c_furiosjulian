
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Regla_LCD_US_interup.h"       /* <= own header */

#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 30000000
/*==================[internal data definition]===============================*/

	bool OnOff=true,HOLD=true,Cm=true,Pg=false,Trasmitir=true;
	uint16_t valor=0;
/*==================[internal functions declaration]=========================*/

void ONOFF(){
	if(OnOff==false){
	OnOff=true;
	LedOn(LED_RGB_B);}
	else{
	LedOff(LED_RGB_B);
	OnOff=false;
	}
}

void Hold(){
	if(HOLD==false){
	LedOff(LED_1);
	HOLD=true;
	TimerStart(TIMER_B);
	}
	else{
	LedOn(LED_1);
	HOLD=false;
	TimerStop(TIMER_B);
	}
}

void ToCm(){
	Cm=true;
	Pg=false;
	LedOff(LED_3);
	LedOn(LED_2);
}

void ToPg(){
	Cm=false;
	Pg=true;
	LedOff(LED_2);
	LedOn(LED_3);
}

/*void LeerValor(){
		if(Cm==true)
				valor=HcSr04ReadDistanceCentimeters();
		if(Pg==true)
				valor=HcSr04ReadDistanceInches();

}*/
void SepararDigitos(uint8_t resul[3]){
	int i=0;
	uint8_t dato;
	dato=valor;
	for(i=0;i<3;i++){
		resul[i]=dato%10;
		dato/=10;
	}
	return;
}

void EnviarValor(){

	if(OnOff==true){
		Trasmitir=true;
	}
	else{
		UartSendByte(SERIAL_PORT_P2_CONNECTOR,0);
	}
}

void nada(){};
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	gpio_t pins[7];
	timer_config MiTimer;
	serial_config puertoconfig;
	SystemClockInit();
	uint8_t dato[3],i;
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;


	MiTimer.timer=TIMER_B;
	MiTimer.period=1000;
	MiTimer.pFunc=EnviarValor;
	puertoconfig.port=SERIAL_PORT_P2_CONNECTOR;
	puertoconfig.baud_rate=9600;
	puertoconfig.pSerial=nada;


	UartInit(&puertoconfig);
	LedsInit();
	SwitchesInit();
	ITSE0803Init(pins);
	HcSr04Init(T_FIL2,T_FIL3);
	TimerInit(&MiTimer);
	TimerStart(TIMER_B);

	LedOn(LED_RGB_B);
	LedOn(LED_2);
	SwitchActivInt(SWITCH_1, ONOFF);
	SwitchActivInt(SWITCH_2, Hold);
	SwitchActivInt(SWITCH_3, ToCm);
	SwitchActivInt(SWITCH_4, ToPg);
	while(1){
		if(Trasmitir==true){


				UartSendString(SERIAL_PORT_P2_CONNECTOR,UartItoa(valor,10));
				UartSendString(SERIAL_PORT_P2_CONNECTOR,"cm\r\n");
				Trasmitir=false;
		}
		if(OnOff==true){
			if(Cm==true)
				valor=HcSr04ReadDistanceCentimeters();
			if(Pg==true)
				valor=HcSr04ReadDistanceInches();
		}


	}
	return 0;
}

    

/*==================[end of file]============================================*/

