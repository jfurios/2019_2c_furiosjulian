/* Parcial del segundo cuatrimestre de 2019 de Electronica Programable
 *
 * Alumno: Furios Julian
 *
 * fecha:28/10/2019
 *
 */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * yyyymmdd v0.0.1 initials initial version
 */

#ifndef PARCIAL
#define PARCIAL

/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef PARCIAL*/

