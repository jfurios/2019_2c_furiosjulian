/* Parcial del segundo cuatrimestre de 2019 de Electronica Programable
 *
 * Alumno: Furios Julian
 *
 * fecha:28/10/2019
 *
 */


/*==================[inclusions]=============================================*/
#include "../inc/Parcial.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
//#include "ili9341.h"
//#include "spi.h"
#include "analog_io.h"
//#include "fonts.h"
/*==================[macros and definitions]=================================*/

#define Max 1
#define Min 2
#define Prom 3
#define muestras 250		//numero de muestras que se toman en 1 seg
#define PresionBaja 50		//valor de presion baja en mmHg
#define PresionAlta 150		//valor de presion alta en mmHg
/*==================[internal data definition]===============================*/
uint16_t ValorLeido,cont=0,mmHg=0,ValoresAcum=0,Maximo=0,Minimo=0;
uint16_t Enviar=1;//variable que sirve para saber que dato se tiene que enviar
/*==================[internal functions declaration]=========================*/

void LeerValor(void){


	AnalogInputReadPolling(CH2,&ValorLeido); //Leo el valor del puerto
	mmHg=(ValorLeido*200)/1024;		//convierto el valor leido a mmHg
	ValoresAcum+=mmHg;				//Acumulo el resultado para hacer el promedio
	if(mmHg>Maximo)					//Busco si es el maximo
		Maximo=mmHg;
	if(mmHg<Minimo)					//Busco si es el minimo
		Minimo=mmHg;
	cont++;

	if(cont==muestras)					//Detengo el muestro si acumulo 1 segundo de valores
		TimerStop(TIMER_A);

}

void Maxi(void){ //enciendo el Led que esta encima del switch presionado
	Enviar=Max;
	LedOn(LED_1);
	LedOff(LED_2);
	LedOff(LED_3);
}

void Mini(void){ //enciendo el Led que esta encima del switch presionado
	Enviar=Min;
	LedOff(LED_1);
	LedOn(LED_2);
	LedOff(LED_3);
}

void Prome(void){ //enciendo el Led que esta encima del switch presionado
	Enviar=Prom;
	LedOff(LED_1);
	LedOff(LED_2);
	LedOn(LED_3);
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	uint16_t Promedio;

	timer_config MiTimer;
	serial_config puertoconfig;
	SystemClockInit();
	analog_input_config AEConf;

	/**Inicializacio de timer para el muestreo de datos*/
	MiTimer.timer=TIMER_A;
	MiTimer.period=4;
	MiTimer.pFunc=LeerValor;
	TimerInit(&MiTimer);

	/**Inicializacion de puerto serie*/
	puertoconfig.port=SERIAL_PORT_P2_CONNECTOR;
	puertoconfig.baud_rate=9600;
	puertoconfig.pSerial=NULL;
	UartInit(&puertoconfig);


	/** Inicializacion de puerto ADC para canal de entrada*/
	AEConf.input=CH1;
	AEConf.mode=AINPUTS_SINGLE_READ;
	AEConf.pAnalogInput=NULL;
	AnalogInputInit(&AEConf);



	/** Inicializacion de LED y Switch de la edu ciaa*/
	LedsInit();
	SwitchesInit();


	/** Inicializacion de display */
	gpio_t pins[7];
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);


	/** Interrupciones para seleccionar la Maximo Minimo Y promedio*/
	SwitchActivInt(SWITCH_2,Maxi);
	SwitchActivInt(SWITCH_3,Mini);
	SwitchActivInt(SWITCH_4,Prome);


	/** Incio del muestreo */
	TimerStart(TIMER_A);

	while(1){
		if(cont==muestras){
			switch(Enviar){
			case Max:
					UartSendString(SERIAL_PORT_PC,UartItoa(Maximo,10));//envio el valor maximo a la pc convertido a ascii
					UartSendString(SERIAL_PORT_PC," Maximo valor de Presion en mmHg\r\n");
					ITSE0803DisplayValue(Maximo);//Escribo el diplay con el valor maximo
			break;

			case Min:
					UartSendString(SERIAL_PORT_PC,UartItoa(Minimo,10));//envio el valor minimo a la pc convertido a ascii
					UartSendString(SERIAL_PORT_PC," Minimo valor de Presion en mmHg\r\n");
					ITSE0803DisplayValue(Minimo);//Escribo el diplay con el valor minimo
			break;

			case Prom:
					Promedio=ValoresAcum/muestras;
					UartSendString(SERIAL_PORT_PC,UartItoa(Promedio,10));//envio el valor promedio a la pc convertido a ascii
					UartSendString(SERIAL_PORT_PC," Valor promedio de Presion en mmHg\r\n");
					ITSE0803DisplayValue(Promedio);//Escribo el diplay con el valor Promedio
			break;

			}

			if(Maximo>PresionAlta){ //Enciendo el Led rojo si la presion esta alta
				LedOn(LED_RGB_R);
				LedOff(LED_RGB_B);
				LedOff(LED_RGB_G);
			}
			else{
				if(Maximo<PresionBaja){ //Enciendo el Led verde si la presion esta baja
					LedOff(LED_RGB_R);
					LedOff(LED_RGB_B);
					LedOn(LED_RGB_G);
				}
				else{					//Enciendo el Led azul si la presion esta en un valor medio
					LedOff(LED_RGB_R);
					LedOn(LED_RGB_B);
					LedOff(LED_RGB_G);
				}

			}

			cont=0; //Vuelo el cotador a 0
			TimerStart(TIMER_A); //enciendo el timer nuevamente
		}


	}

}

    

/*==================[end of file]============================================*/

