
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Osciloscopio.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
//#include "DisplayITS_E0803.h"
//#include "hc_sr4.h"
#include "delay.h"
#include "led.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "ili9341.h"
#include "spi.h"
#include "analog_io.h"
#include "fonts.h"

/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
	uint16_t ValorLeido,y,x,y0=0,x0=0;
	uint8_t tiempo=0, ganancia=1, Cont;
	bool GanFlag=false,TimFlag=false;
/*==================[internal functions declaration]=========================*/
void EnviarValor(void){

	if(Cont==tiempo){
	AnalogInputReadPolling(CH2,&ValorLeido);
	x=(ValorLeido*229)/1024;
	ILI9341DrawLine(y,x,y0,x0,ILI9341_PURPLE);
	//ILI9341DrawPixel(y,x,ILI9341_PURPLE);
	x0=x;
	y0=y;
	y++;
	if(y>=ILI9341_HEIGHT){
		LimpiarPantalla();
		y=0;
		y0=0;
	}
	Cont=0;
	}
	else
	Cont++;
}

void AC_DC(void){
	bool OnOff;
	GPIOToggle(GPIO3);
	GPIOState(GPIO3,OnOff);
	if(OnOff){
		LedOn(LED_RGB_B);
		LedOff(LED_RGB_G);
	}
	else{
		LedOn(LED_RGB_G);
		LedOff(LED_RGB_B);
	}
}

void BaseTiempo(void){
 tiempo++;
if(tiempo>=5)
	tiempo=0;
TimFlag=true;
}

void GanMas(void){
if(ganancia<=2)
	ganancia++;
else
	ganancia=1;
GanFlag=true;
}

void GanMenos(void){
if(ganancia>=1)
	ganancia--;
else
	ganancia=3;
GanFlag=true;
}

void LimpiarPantalla(void){
	ILI9341DrawFilledRectangle(0,0,320,229,ILI9341_BLACK);
	ILI9341DrawLine(106,0,106,229,ILI9341_YELLOW);
	ILI9341DrawLine(212,0,212,229,ILI9341_YELLOW);
	ILI9341DrawLine(0,77,320,77,ILI9341_YELLOW);
	ILI9341DrawLine(0,154,320,154,ILI9341_YELLOW);
}

void TiempoDiv(uint16_t val){
	ILI9341DrawFilledRectangle(0,230,160,240,ILI9341_BLACK);
	ILI9341DrawString(0,230,"Tim/Div: ",&font_7x10,ILI9341_YELLOW,ILI9341_BLACK);
	ILI9341DrawInt(65,230,&val,3,&font_7x10,ILI9341_YELLOW,ILI9341_BLACK);
}

void VoltsDiv(uint16_t val){
	ILI9341DrawFilledRectangle(160,230,320,240,ILI9341_BLACK);
	ILI9341DrawString(160,230,"V/Div: ",&font_7x10,ILI9341_YELLOW,ILI9341_BLACK);
	ILI9341DrawInt(210,230,&val,1,&font_7x10,ILI9341_YELLOW,ILI9341_BLACK);
}

void Gan(void){
	switch(ganancia){

	case 1:
		GPIOOn(GPIO1);
		GPIOOn(GPIO0);
		GPIOOff(GPIO2);
	break;
	case 2:
		GPIOOn(GPIO2);
		GPIOOn(GPIO0);
		GPIOOff(GPIO1);
	break;
	case 3:
		GPIOOn(GPIO1);
		GPIOOn(GPIO2);
		GPIOOff(GPIO0);
	break;

	}
	VoltsDiv(ganancia);
}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	timer_config MiTimer;
//	serial_config puertoconfig;

	SystemClockInit();
	//spiConfig_t SpiConf;
	analog_input_config AEConf,ATConf;

	/**Inicializacio de timer para el muestreo de datos*/
	MiTimer.timer=TIMER_A;
	MiTimer.period=10;
	MiTimer.pFunc=EnviarValor;
	TimerInit(&MiTimer);

//	puertoconfig.port=SERIAL_PORT_P2_CONNECTOR;
//	puertoconfig.baud_rate=9600;
//	puertoconfig.pSerial=NULL;

	/** Inicializacion de puerto ADC para canal de entrada*/
	AEConf.input=CH2;
	AEConf.mode=AINPUTS_SINGLE_READ;
	AEConf.pAnalogInput=NULL;
	AnalogInputInit(&AEConf);

	/** Inicializacion de puerto ADC para canal de Trigger*/
	ATConf.input=CH1;
	ATConf.mode=AINPUTS_SINGLE_READ;
	ATConf.pAnalogInput=NULL;
	//AnalogInputInit(&ATConf);

//	UartInit(&puertoconfig);

	/** Inicializacion de LED y Switch de la edu ciaa*/
	LedsInit();
	SwitchesInit();


	/** Inicializacion de display color */
	ILI9341Init(SPI_1,GPIO8,GPIO6,GPIO5);
	GPIOInit(GPIO7,GPIO_OUTPUT);
	GPIOOn(GPIO7);

	/** Inicializacion de GPIO necesarios para control de ganancia y AC_CD*/
	GPIOInit(GPIO3,GPIO_OUTPUT);
	GPIOInit(GPIO2,GPIO_OUTPUT);
	GPIOInit(GPIO1,GPIO_OUTPUT);
	GPIOInit(GPIO0,GPIO_OUTPUT);




	/** Interrupciones para seleccionar la ganancia y modo AC_DC*/
	SwitchActivInt(SWITCH_1,AC_DC);
	SwitchActivInt(SWITCH_2,BaseTiempo);
	SwitchActivInt(SWITCH_3,GanMas);
	SwitchActivInt(SWITCH_4,GanMenos);

	/** Inicializacion de pantalla*/
	ILI9341Rotate(ILI9341_Landscape_1);
	LimpiarPantalla();
	Gan();
	TiempoDiv(106);

	/** Incio del muestreo */
	TimerStart(TIMER_A);

	uint16_t ValorTrigger;

	while(1){
		if(y==0){
			TimerStop(TIMER_A);
			AnalogInputInit(&AEConf);
			AnalogInputReadPolling(CH2,&ValorLeido);
			AnalogInputInit(&ATConf);
			AnalogInputReadPolling(CH1,&ValorTrigger);
			if(ValorLeido==ValorTrigger){
				x=(ValorLeido*229)/1024;
				ILI9341DrawLine(y0,x0,y,x,ILI9341_PURPLE);
				x0=x;
				y0=y;
				y++;
				TimerStart(TIMER_A);
				AnalogInputInit(&AEConf);
			}
		}
		if(GanFlag){
			GanFlag=false;
			Gan();
		}
		if(TimFlag){
			TimFlag=false;
			TiempoDiv(tiempo*106);
		}
	}


	return 0;
}

    

/*==================[end of file]============================================*/

