
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Regla_LCD_US.h"       /* <= own header */

#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "delay.h"
#include "led.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 30000000
#define true 1
#define false 0
/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint16_t valor=0,tecla;
	gpio_t pins[7];
	SystemClockInit();
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	LedsInit();
	SwitchesInit();
	ITSE0803Init(pins);
	HcSr04Init(T_FIL2,T_FIL3);
	int OnOff=true,HOLD=true,Cm=true,Pg=false;
	LedOn(LED_RGB_B);
	LedOn(LED_2);
	while(1){

		if(OnOff==true){
			if(HOLD==true){
				if(Cm==true)
						valor=HcSr04ReadDistanceCentimeters();
				if(Pg==true)
						valor=HcSr04ReadDistanceInches();
			}
		ITSE0803DisplayValue(valor);
		DelayMs(150);
		tecla=SwitchesRead();
		}
		if(OnOff==false){
			ITSE0803White();
			DelayMs(150);
			tecla=SwitchesRead();
		//DelayMs(100);
		}
		/*if(tecla==SWITCH_1)
			i=1;
		while(i==1){
			DelayMs(100);
			tecla=SwitchesRead();
			if(tecla==SWITCH_1)
				i=0;
			DelayMs(100);
		}*/
		switch(tecla){
		case SWITCH_1:
			if(OnOff==true){
				OnOff=false;
				LedOff(LED_RGB_B);
			}
			else{
				OnOff=true;
			LedOn(LED_RGB_B);
			}
			break;
		case SWITCH_2:
			if(HOLD==true){
				HOLD=false;
				LedOn(LED_1);
			}
			else{
				HOLD=true;
				LedOff(LED_1);
			}
			break;
		case SWITCH_3:
			Cm=true;
			Pg=false;
			LedOn(LED_2);
			LedOff(LED_3);
		break;
		case SWITCH_4:
			Cm=false;
			Pg=true;
			LedOff(LED_2);
			LedOn(LED_3);
		break;
		}
	}


	return 0;
}

    

/*==================[end of file]============================================*/

